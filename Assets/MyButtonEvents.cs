using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MyButtonEvents : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent OnPressed;
    public UnityEvent OnReleased;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        OnPressed?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnReleased?.Invoke();
    }
}
