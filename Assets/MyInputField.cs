using UnityEngine;
using UnityEngine.EventSystems;

public class MyInputField : InputFieldOriginal
{
    private bool _keyWasPressed;
    
    private void KeyPressedRoutine(Event evt)
    {
        _keyWasPressed = true;
        evt.type = EventType.KeyDown;
        
        KeyPressed(evt);
        UpdateLabel();
        OnSelect(new PointerEventData(EventSystem.current));
    }
    
    public void KeyPressed(char c)
    {
        var evt = new Event
        {
            character = c,
        };

        KeyPressedRoutine(evt);
    }
    
    public void BackspacePressed()
    {
        var evt = new Event
        {
            keyCode = KeyCode.Backspace,
        };

        KeyPressedRoutine(evt);
    }
    
    public void LeftArrowPressed()
    {
        var evt = new Event
        {
            keyCode = KeyCode.LeftArrow,
        };

        KeyPressedRoutine(evt);
    }
    
    public void RightArrowPressed()
    {
        var evt = new Event
        {
            keyCode = KeyCode.RightArrow,
        };

        KeyPressedRoutine(evt);
    }
    
    public override void OnDeselect(BaseEventData eventData)
    {
        if (_keyWasPressed)
        {
            _keyWasPressed = false;
            return;
        }

        base.OnDeselect(eventData);
    }
}