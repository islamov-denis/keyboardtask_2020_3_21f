﻿using System;
using System.Collections;
using UnityEngine;

public class VirtualKeyboard : MonoBehaviour
{
    public MyInputField InputField;
    private Coroutine _keyPressCoroutine;
    
    [SerializeField] private float _keyPressDelayInSeconds = 0.35f;
    
    private Action _delegateToKeyPress;
    
    public void Start()
    {
        InputField.BackspacePressed();
    }

    public void KeyPress(string c)
    {
        //InputField.text += c;
        InputField.KeyPressed(c.ToCharArray()[0]);
    }

    public void KeyLeft()
    {
        InputField.LeftArrowPressed();
    }

    public void KeyRight()
    {
        InputField.RightArrowPressed();
    }

    public void KeyDelete()
    {
        InputField.BackspacePressed();
    }
    
    public void KeyPressOnPointerDown(string c)
    {
        _delegateToKeyPress = c switch
        {
            "Backspace" => KeyDelete,
            "Left" => KeyLeft,
            "Right" => KeyRight,
            _ => null
        };

        _keyPressCoroutine = StartCoroutine(KeyPressRepeat(c));
    }
    
    public void KeyPressOnPointerUp()
    {
        StopCoroutine(_keyPressCoroutine);
        _keyPressCoroutine = null;
    }
    
    private IEnumerator KeyPressRepeat(string s)
    {
        while (true)
        {
            if (_delegateToKeyPress == null)
            {
                KeyPress(s);
            }
            else
            {
                _delegateToKeyPress.Invoke();
            }

            yield return new WaitForSeconds(_keyPressDelayInSeconds);
        }
    }
}
